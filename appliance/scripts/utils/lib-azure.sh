#!/usr/bin/env bash

function az-login() {
    azClientId="$1"
    azClientSecret="$2"
    azTennantId="$3"
    az login --service-principal --username "$azClientId" --password "$azClientSecret" --tenant "$azTennantId"
}

function az-create-rg() {
    rgName="$1"
    rgLocation="$2"
    az group create --name "$rgName" --location "$rgLocation"
}

# [Azure Note] While it is feasible to configure LVM on any disk attached to the virtual machine,
# by default most cloud images will not have LVM configured on the OS disk.
# This is to prevent problems with duplicate volume groups if the OS disk is ever attached
# to another VM of the same distribution and type, i.e. during a recovery scenario.
# Therefore it is recommended only to use LVM on the data disks.

function az-deploy-simple-vm() {
    rgName="$1"
    vmName="$2"
    sku="$3"
    username="$4"
    password="$5"

    nsgRule="SSH"
    if echo "$sku" | grep -i "windows" > /dev/null 2>&1; then
        nsgRule="RDP"
    fi
    az vm create --name "$vmName" \
        --resource-group "$rgName" \
        --image "$sku" \
        --size Standard_A1 \
        --authentication-type password \
        --nsg-rule "$nsgRule" \
        --admin-username "$username" \
        --admin-password "$password"
}

function az-get-vm-run-state() {
    rgName="$1"
    vmName="$2"
    if ! az vm show -g "$rgName" -n "$vmName" > /dev/null 2>&1; then
        echo "not found"
        return
    fi
    az vm show -g "$rgName" -n "$vmName" -d -o yaml | grep powerState | cut -d" " -f2,3
}

function az-get-vm-provision-state() {
    rgName="$1"
    vmName="$2"
    if ! az vm show -g "$rgName" -n "$vmName" > /dev/null 2>&1; then
        echo "not found"
        return
    fi
    az vm show -g "$rgName" -n "$vmName" -d -o yaml | grep provisioningState | cut -d" " -f2,3
}

function az-cleanup-rg() {
    rgName="$1"
    if ! az group show -n "$rgName" > /dev/null 2>&1; then
        return 0
    fi
    az group delete --yes --no-wait --name "$rgName"
    retryCount=0
    while [[ $retryCount -lt 5 ]]; do
        if [ "$(az group show -n "$rgName" -o yaml | grep provisioningState | grep -c Deleting)" != "0" ]; then
            echo "Deleting resource-group $rgName"
            return 0
        else
            let retryCount+=1
            az group delete --yes --no-wait --name "$rgName"
        fi
    done
    echo "ERROR: Could not trigger deletion for the Azure resource-group $rgName."
    return 1
}