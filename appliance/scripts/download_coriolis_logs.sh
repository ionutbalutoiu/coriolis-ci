#!/usr/bin/env bash
set -o errexit
set -o pipefail

if [[ -z $LOGS_DIR ]]; then echo "ERROR: The env variable LOGS_DIR is not set"; exit 1; fi

run_cmd_with_retry() {
    local RETRIES=$1
    local WAIT_SLEEP=$2
    local TIMEOUT=$3

    shift && shift && shift

    for i in $(seq 1 $RETRIES); do
        timeout $TIMEOUT ${@} && break || \
        if [ $i -eq $RETRIES ]; then
            echo "Error: Failed to execute \"$@\" after $i attempts"
            return 1
        else
            echo "Failed to execute \"$@\". Retrying in $WAIT_SLEEP seconds..."
            sleep $WAIT_SLEEP
        fi
    done
    echo Executed \"$@\" $i times;
}

LOG_LIST=$(run_cmd_with_retry 10 10 10 coriolis log list -f value | grep "^coriolis-")

mkdir -p $LOGS_DIR
for LOG_NAME in $LOG_LIST; do
    run_cmd_with_retry 10 10 10 coriolis log download --out-file "${LOGS_DIR}/${LOG_NAME}.log" "$LOG_NAME"
done
