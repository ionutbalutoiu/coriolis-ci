#!/usr/bin/env bash
set -o errexit
set -o pipefail

if [[ -z $APPLIANCE_IP ]]; then echo "ERROR: The env variable APPLIANCE_IP is not set"; exit 1; fi
if [[ -z $GENERATOR_BIN_URL ]]; then echo "ERROR: The env variable GENERATOR_BIN_URL is not set"; exit 1; fi

export TIMEOUT=1800

cd ~

echo "Downloading licensing generator binary"
SECONDS=0
while [[ $SECONDS -lt $TIMEOUT ]]; do
    curl --silent $GENERATOR_BIN_URL -o generator && break || sleep 10
done

chmod +x generator

SECONDS=0
while [[ $SECONDS -lt $TIMEOUT ]]; do
    APPLIANCE_ID=`curl --fail --insecure --silent https://${APPLIANCE_IP}/licensing/licence-status | jq -r '.licence_status.appliance_id'` \
        && break || sleep 10
done

echo "Appliance ID: $APPLIANCE_ID"

SECONDS=0
while [[ $SECONDS -lt $TIMEOUT ]]; do
    echo "Trying to apply a new Coriolis licence"
    rm -f licence.pem
    ./generator generate --appliance-id $APPLIANCE_ID --output-file licence.pem \
                         --expiry-days 30 --migrations 100 --replicas 100
    curl --fail --insecure --silent -X POST -H "Content-Type:application/x-pem-file" --data-binary @licence.pem \
        https://${APPLIANCE_IP}/licensing/licences && break || sleep 10
done
