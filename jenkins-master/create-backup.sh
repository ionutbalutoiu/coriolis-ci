#!/usr/bin/env bash
set -e

BACKUPS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/backup"
BACKUP_NAME=$(date "+%m_%d_%y-%H_%M")
VOLUME_NAME="jenkins-data"
IMAGE_NAME="jenkins-master"


# - Check if the Jenkins data volume exists
if [[ "$(docker volume ls -q -f name=$VOLUME_NAME)" = "" ]]; then
    echo "ERROR: Docker volume $VOLUME_NAME doesn't exist"
    exit 1
fi

# - Check if the Jenkins container image exists
if [[ "$(docker images -q $IMAGE_NAME)" = "" ]]; then
    echo "ERROR: Docker container image $IMAGE_NAME doesn't exist"
    exit 1
fi

# - Create the backup dir
rm -rf $BACKUPS_DIR/$BACKUP_NAME
mkdir -p $BACKUPS_DIR/$BACKUP_NAME

# - Backup the Jenkins data volume
docker run --rm -v jenkins-data:/jenkins-data -v $BACKUPS_DIR/$BACKUP_NAME:/mnt alpine tar czf /mnt/jenkins-data.tar.gz jenkins-data

# - Backup the Jenkins container image
docker save -o $BACKUPS_DIR/$BACKUP_NAME/jenkins-container-image.tar $IMAGE_NAME
gzip $BACKUPS_DIR/$BACKUP_NAME/jenkins-container-image.tar
