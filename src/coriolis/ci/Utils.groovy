package coriolis.ci


def run_coriolis_tools_script(String script, Boolean return_stdout = false) {
    docker.withRegistry(global_vars.DOCKER_REGISTRY, global_vars.DOCKER_REGISTRY_CREDS_ID) {
        def image = docker.image(global_vars.DOCKER_CORIOLIS_TOOLS_IMAGE)
        image.pull()
        image.inside {
            def script_code = """#!/usr/bin/env bash
                                 set -o errexit
                                 set -o pipefail
                                 source /etc/profile
                                 ${script}"""
            if (return_stdout) {
                def output = sh(script: script_code, returnStdout: true)
                println(output)
                return output
            }
            sh(script: script_code)
        }
    }
}

def run_coriolis_tools_remote_command(LinkedHashMap remote, String command, String docker_args = "") {
    withCredentials([usernamePassword(credentialsId: global_vars.DOCKER_REGISTRY_CREDS_ID,
                                      usernameVariable: "DOCKER_USER_NAME",
                                      passwordVariable: "DOCKER_USER_PASS")]) {
        try {
            sshCommand(
                remote: remote,
                failOnError: true,
                command: "docker login ${global_vars.DOCKER_REGISTRY} -u ${DOCKER_USER_NAME} -p '${DOCKER_USER_PASS}'")
            sshCommand(
                remote: remote,
                failOnError: true,
                command: "docker run --rm --network host ${docker_args} ${global_vars.DOCKER_CORIOLIS_TOOLS_IMAGE} ${command}")
        } finally {
            sshCommand(
                remote: remote,
                failOnError: true,
                command: "docker logout ${global_vars.DOCKER_REGISTRY}")
        }
    }
}

return this
